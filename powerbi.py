#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, mariadb
try:
  import simplejson as json
except ImportError:
    import json


# Connect to MariaDB Platform
try:
    conn = mariadb.connect(
        user="login",
        password="password",
        host="localhost",
        port=3306,
        database="db"

    )
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

# Get Cursor
cur = conn.cursor(dictionary=True)

# Companies
cur.execute("""
    SELECT id, inn, user_id, name, phone, address, 
    DATE_FORMAT(created_at, '%Y-%m-%d %T') AS created_at,
    DATE_FORMAT(updated_at, '%Y-%m-%d %T') AS updated_at,
    kpp, ogrn, full_with_opf, management_name, management_post, okved,
    DATE_FORMAT(ogrn_date, '%Y-%m-%d') AS ogrn_date,
    district_id
    FROM companies;
""")

result = cur.fetchall()


with open('/tmp/companies.json', 'w', encoding='utf-8') as f:
    json.dump(result, f, ensure_ascii=False)
f.close()

# Forms
cur.execute("""
    SELECT id, name, access, is_active,
    DATE_FORMAT(created_at, '%Y-%m-%d %T') AS created_at,
    DATE_FORMAT(updated_at, '%Y-%m-%d %T') AS updated_at,
    period_type, period_data, category_id 
    FROM forms;
""")

result = cur.fetchall()

doc=[]


for x in result:
    dictpst=json.loads(x['period_data'])
    x['period_data'] = dictpst
    doc.append(x)

with open('/tmp/forms.json', 'w', encoding='utf-8') as f:
    json.dump(doc, f, ensure_ascii=False)
f.close()

# Stats
cur.execute("""
    SELECT id, form_id, status_id, company_id, user_id,
    DATE_FORMAT(date, '%Y-%m-%d') AS date,
    DATE_FORMAT(report_date, '%Y-%m-%d') AS report_date,
    data,
    DATE_FORMAT(created_at, '%Y-%m-%d %T') AS created_at,
    DATE_FORMAT(updated_at, '%Y-%m-%d %T') AS updated_at,
    period_id, autoreport    
    FROM stats;
""")

result = cur.fetchall()

doc=[]
count=0

for x in result:
    count=count+1
    dictpst=json.loads(x['data'])
    x['data'] = dictpst
    doc.append(x)

print(count)
with open('/tmp/stats.json', 'w', encoding='utf-8') as f:
    json.dump(doc, f, ensure_ascii=False)
f.close()

# Close Connection
conn.close()

def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
